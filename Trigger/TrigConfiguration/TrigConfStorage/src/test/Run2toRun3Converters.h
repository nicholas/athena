/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "TrigConfData/HLTMenu.h"
#include "TrigConfHLTData/HLTFrame.h"
void convertRun2HLTtoRun3(const TrigConf::HLTFrame* frame, const std::string& filename);